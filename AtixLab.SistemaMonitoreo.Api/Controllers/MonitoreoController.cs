﻿using AtixLabs.SistemaMonitoreo.Model;
using AtixLabs.SistemaMonitoreo.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AtixLab.SistemaMonitoreo.Api.Controllers
{
    [Route("api/monitoreo")]
    [ApiController]
    public class MonitoreoController : ControllerBase
    {
        private readonly IMensajesService mensajesService;

        private readonly ILogger logger;

        public MonitoreoController(IMensajesService mensajesService, ILoggerFactory loggerFactory)
        {
            this.mensajesService = mensajesService;

            this.logger = loggerFactory.CreateLogger<IMensajesService>();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Mensaje mensaje)
        {
            this.logger.LogInformation($"Se agrego a la cola de procesamiento la solicitud con fecha {mensaje.Fecha} en al fecha {DateTime.Now.ToString("yyyy-MM-dd")}");

            // logeo el procesamiento pero ademas puedo obtener mas informacion acerca de quien me envio la solicitud

            await this.mensajesService.GuardarMensajeAsync(mensaje);

            return Ok();
        }
    }
}