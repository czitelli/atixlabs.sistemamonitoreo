﻿using System;

namespace AtixLabs.SistemaMonitoreo.Model
{
    public interface IProcessConfiguration
    {
        String TiempoEjecucion { get; set; }
        String StorageAccount { get; set; }
        String MessageQueueName { get; set; }
        Decimal PromedioMaximoAdmitido { get; set; }
        Int32 DifenciaMinimoMaximoAdmitida { get; set; }
        String PromedioMaximoAdmitidoSuperadoMessage { get; set; }
        String DiferenciaEntreMinimoMaximaSuperadaMessage { get; set; }
    }

    public class ProcessConfiguration : IProcessConfiguration
    {
        public String StorageAccount { get; set; }
        public String TiempoEjecucion { get; set; }
        public String MessageQueueName { get; set; }
        public Decimal PromedioMaximoAdmitido { get; set; }
        public Int32 DifenciaMinimoMaximoAdmitida { get; set; }
        public String PromedioMaximoAdmitidoSuperadoMessage { get; set; }
        public String DiferenciaEntreMinimoMaximaSuperadaMessage { get; set; }
    }
}