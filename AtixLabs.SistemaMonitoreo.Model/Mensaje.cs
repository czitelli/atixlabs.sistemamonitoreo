﻿using System;

namespace AtixLabs.SistemaMonitoreo.Model
{
    public class Mensaje
    {
        public Int64 Id { get; set; }
        public DateTime Fecha { get; set; }
        public Int32[] Mediciones { get; set; }
        public Boolean Procesado { get; set; }
    }
}