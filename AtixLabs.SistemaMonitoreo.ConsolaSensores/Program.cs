﻿using AtixLabs.SistemaMonitoreo.Model;
using AtixLabs.SistemaMonitoreo.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.WindowsAzure.Storage;
using System;

namespace AtixLabs.SistemaMonitoreo.ConsolaSensores
{
    internal class Program
    {
        private static IMensajesService MensajesService { get; set; }
        private static Random Random { get; set; }

        private static void Main(string[] args)
        {
            //configuro la injection
            Initialize();

            Console.WriteLine("Enviando 20 Mediciones de Sensores");

            for (int i = 0; i < 20; i++)
            {
                Mensaje messaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = i,
                    Mediciones = GetMedicionesRandom()
                };

                MensajesService.GuardarMensajeAsync(messaje);
            }

            Console.WriteLine("Enviado Mediciones de Sensores");
        }

        public static void Initialize()
        {
            Random = new Random(10);
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IMensajesService, MensajesService>(s =>
                {
                    return new MensajesService(CloudStorageAccount.DevelopmentStorageAccount, "message-queue");
                })
                .BuildServiceProvider();
            MensajesService = serviceProvider.GetService<IMensajesService>();
        }

        private static Int32[] GetMedicionesRandom()
        {
            return new Int32[]
                    {
                        GetRandomNum(),
                        GetRandomNum(),
                        GetRandomNum(),
                        GetRandomNum(),
                    };
        }

        private static Int32 GetRandomNum()
        {
            return Random.Next(1, 20);
        }
    }
}