﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AtixLabs.CuentaBancaria.Modificada;

namespace AtixLabs.CuentaBancaria.Tests
{
    [TestClass]
    public class CajaAhorroTests
    {
        protected CajaAhorro cajaAhorro;

        [TestInitialize]
        public void Initialize()
        {
            this.cajaAhorro = new CajaAhorro(3000, "Cristian");
        }

        [TestClass]
        public class ElMetodo_Depositar: CajaAhorroTests
        {
            [TestMethod]
            public void Al_Depositar_El_Saldo_Se_Incrementa_Lo_Depositado()
            {
                //Arrange
                Decimal saldoOriginal = this.cajaAhorro.Saldo;
                Decimal saldoIncrementado = 200;

                //Act
                this.cajaAhorro.depositar(saldoIncrementado);

                //Assert
                Assert.AreEqual(saldoOriginal + saldoIncrementado, this.cajaAhorro.Saldo);
            }
        }

        [TestClass]
        public class ElMetodo_Extraer : CajaAhorroTests
        {
            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public void Al_Extraer_Un_Monto_Por_Encima_Del_Saldo_Lanza_Exception()
            {
                //Arrange
                Decimal montoExtraccion = this.cajaAhorro.Saldo + 100;

                //Act
                this.cajaAhorro.extraer(montoExtraccion);

                //Assert
            }

            [TestMethod]
            public void Al_Extraer_Un_Monto_Por_Debajo_Del_Saldo_Decrementa_El_Saldo()
            {
                //Arrange
                Decimal saldoOriginal = this.cajaAhorro.Saldo;
                Decimal montoExtraccion = saldoOriginal - 1;

                //Act
                this.cajaAhorro.extraer(montoExtraccion);

                //Assert
                Assert.AreEqual(saldoOriginal - montoExtraccion, this.cajaAhorro.Saldo);
            }
        }
    }
}
