﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AtixLabs.CuentaBancaria.Modificada;

namespace AtixLabs.CuentaBancaria.Tests
{
    [TestClass]
    public class CuentaCorrienteTests
    {
        protected CuentaCorriente cuentaCorriente;

        [TestInitialize]
        public void Initialize()
        {
            this.cuentaCorriente = new CuentaCorriente(3000, "Cristian", 2500);
        }

        [TestClass]
        public class ElMetodo_Depositar: CuentaCorrienteTests
        {
            [TestMethod]
            public void Al_Depositar_El_Saldo_Se_Incrementa_Lo_Depositado()
            {
                //Arrange
                Decimal saldoOriginal = this.cuentaCorriente.Saldo;
                Decimal saldoIncrementado = 200;

                //Act
                this.cuentaCorriente.depositar(saldoIncrementado);

                //Assert
                Assert.AreEqual(saldoOriginal + saldoIncrementado, this.cuentaCorriente.Saldo);
            }
        }

        [TestClass]
        public class ElMetodo_Extraer : CuentaCorrienteTests
        {
            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public void Al_Extraer_Un_Monto_Por_Encima_Del_Descubierto_Lanza_Exception()
            {
                //Arrange
                Decimal montoExtraccion = this.cuentaCorriente.Saldo + (this.cuentaCorriente.Descubierto + 100);

                //Act
                this.cuentaCorriente.extraer(montoExtraccion);

                //Assert
            }

            [TestMethod]
            public void Al_Extraer_Un_Monto_Por_Encima_Del_Saldo_Pero_No_El_Descubierto_Decrementa_El_Saldo()
            {
                //Arrange
                Decimal saldoOriginal = this.cuentaCorriente.Saldo;
                Decimal montoExtraccion = saldoOriginal + 1;

                //Act
                this.cuentaCorriente.extraer(montoExtraccion);

                //Assert
                Assert.AreEqual(saldoOriginal - montoExtraccion, this.cuentaCorriente.Saldo);
            }

            [TestMethod]
            public void Al_Extraer_Un_Monto_Por_Debajo_Del_Descubierto_Decrementa_El_Saldo()
            {
                //Arrange
                Decimal saldoOriginal = this.cuentaCorriente.Saldo;
                Decimal montoExtraccion = saldoOriginal - 1;

                //Act
                this.cuentaCorriente.extraer(montoExtraccion);

                //Assert
                Assert.AreEqual(saldoOriginal - montoExtraccion, this.cuentaCorriente.Saldo);
            }
        }
    }
}
