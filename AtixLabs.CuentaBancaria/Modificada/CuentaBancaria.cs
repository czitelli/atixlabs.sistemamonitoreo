﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtixLabs.CuentaBancaria.Modificada
{
    public abstract class CuentaBancaria
    {
        public String Titular { get; set; }

        public Int64 NumeroCuenta { get; set; }

        public Decimal Saldo { get; set; }

        public CuentaBancaria(Int64 numeroCuenta, String titular)
        {
            this.NumeroCuenta = numeroCuenta;

            this.Titular = titular;

            this.Saldo = 0;
        }

        public void depositar(Decimal monto)
        {
            this.Saldo += monto;
        }

        public void extraer(Decimal monto)
        {
            if (this.EvaluarExtraccion(monto))
            {
                throw new Exception("No hay dinero suficiente");
            }

            this.Saldo -= monto;
        }

        protected abstract Boolean EvaluarExtraccion(Decimal monto);
    }
}

