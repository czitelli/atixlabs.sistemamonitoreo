﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtixLabs.CuentaBancaria.Modificada
{
    public class CajaAhorro : CuentaBancaria
    {
        public CajaAhorro(long numeroCuenta, string titular) : base(numeroCuenta, titular)
        {
        }

        protected override bool EvaluarExtraccion(Decimal monto)
        {
            return monto > this.Saldo;
        }
    }
}

