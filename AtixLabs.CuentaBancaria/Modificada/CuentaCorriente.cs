﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtixLabs.CuentaBancaria.Modificada
{
    public class CuentaCorriente : CuentaBancaria
    {
        public Decimal Descubierto { get; set; }

        public CuentaCorriente(Int64 numeroCuenta, String titular, Decimal descubierto) : base(numeroCuenta, titular)
        {
            this.Descubierto = descubierto;
        }

        protected override bool EvaluarExtraccion(Decimal monto)
        {
            return monto > this.Saldo + this.Descubierto;
        }
    }
}

