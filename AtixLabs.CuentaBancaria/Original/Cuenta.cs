﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtixLabs.Original
{
    public class Cuenta
    {
        public const int CTA_CORRIENTE = 0;
        public const int CAJA_AHORRO = 1;
        private int tipo;
        private long numeroCuenta;
        private String titular;
        private long saldo;
        private long descubiertoAcordado;

        public Cuenta(int tipo, long nCuenta, String titular, long descAcordado)
        {
            this.tipo = tipo;
            this.numeroCuenta = nCuenta;
            this.titular = titular;
            if (tipo == CTA_CORRIENTE)
                this.descubiertoAcordado = descAcordado;
            else this.descubiertoAcordado = 0;

            saldo = 0;
        }

        public Cuenta(int tipo, long numeroCuenta, String titular)
        {
            this.tipo = tipo;
            this.numeroCuenta = numeroCuenta;
            this.titular = titular;
            this.descubiertoAcordado = 0;
            saldo = 0;
        }

        public void depositar(long monto)
        {
            saldo += monto;
        }

        public void extraer(long monto)
        {

            switch (tipo) {
                case CAJA_AHORRO:
                    if (monto > saldo)
                        throw new Exception("No hay dinero suficiente");
                    break;
                case CTA_CORRIENTE: if (monto > saldo + descubiertoAcordado)
                        throw new Exception("No hay dinero suficiente");
                    break;
            }
            saldo -= monto;
        }
    }
}

