﻿using AtixLabs.SistemaMonitoreo.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public class EvaluacionPromedioService : EvaluacionServiceBase, IEvaluacionPromedioService
    {
        private readonly Decimal promedioMaximoAdmitido; //Configurable

        public EvaluacionPromedioService(String message, Decimal promedioMaximoAdmitido, ILoggerFactory logger) : base(message, logger)
        {
            this.promedioMaximoAdmitido = promedioMaximoAdmitido;
        }

        protected override Boolean DebeInformarMensaje(Mensaje mensaje)
        {
            var promedio = mensaje.Mediciones.Average(m => m);

            return this.promedioMaximoAdmitido < Convert.ToDecimal(promedio);
        }
    }
}