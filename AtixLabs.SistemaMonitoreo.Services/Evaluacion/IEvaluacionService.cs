﻿using AtixLabs.SistemaMonitoreo.Model;
using System;
using System.Threading.Tasks;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public interface IEvaluacionService
    {
        Task<Boolean> Evaluar(Mensaje mensaje);
    }
}