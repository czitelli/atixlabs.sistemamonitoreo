﻿using AtixLabs.SistemaMonitoreo.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public class EvaluacionServiceComposite : IEvaluacionServiceComposite
    {
        private readonly IEnumerable<IEvaluacionService> evaluaciones;

        public EvaluacionServiceComposite(IEvaluacionPromedioService evaluacionPromedio, IEvaluacionDiferenciaMinimoMaximoService evaluacionDiferenciaMinimoMaximoService)
        {
            this.evaluaciones = new List<IEvaluacionService> { evaluacionPromedio, evaluacionDiferenciaMinimoMaximoService };
        }

        public async Task<Boolean> Evaluar(Mensaje mensaje)
        {
            var result = true;
            foreach (var evaluacion in this.evaluaciones)
            {
                result = result && await evaluacion.Evaluar(mensaje);
            }
            return result;
        }
    }
}