﻿using AtixLabs.SistemaMonitoreo.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public abstract class EvaluacionServiceBase : IEvaluacionService
    {
        private readonly String message; //Configurable

        private readonly ILogger logger;

        public EvaluacionServiceBase(String message, ILoggerFactory logger)
        {
            this.message = message;

            this.logger = logger.CreateLogger("IEvaluacionService");
        }

        protected abstract Boolean DebeInformarMensaje(Mensaje mensaje);

        public async Task<Boolean> Evaluar(Mensaje mensaje)
        {
            //Puedo guardar el mensaje en algun lado, devolver etc. por ahora lo logueo
            if (this.DebeInformarMensaje(mensaje))
                return await Task.Run(() => { this.Informar(); return false; });
            return true;
        }

        public virtual void Informar()
        {
            logger.LogInformation(this.message);
        }
    }
}