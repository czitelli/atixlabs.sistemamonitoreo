﻿using AtixLabs.SistemaMonitoreo.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public class EvaluacionDiferenciaMinimoMaximoService : EvaluacionServiceBase, IEvaluacionDiferenciaMinimoMaximoService
    {
        private readonly Int32 diferenciaMaximaAdmitida; //Configurable

        public EvaluacionDiferenciaMinimoMaximoService(String message, Int32 diferenciaMaximaAdmitida, ILoggerFactory logger) : base(message, logger)
        {
            this.diferenciaMaximaAdmitida = diferenciaMaximaAdmitida;
        }

        protected override Boolean DebeInformarMensaje(Mensaje mensaje)
        {
            var dif = mensaje.Mediciones.Max(m => m) - mensaje.Mediciones.Min(m => m);

            return this.diferenciaMaximaAdmitida < dif;
        }
    }
}