﻿using AtixLabs.SistemaMonitoreo.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public class DependencyInjector
    {
        private readonly IProcessConfiguration configuration;

        public DependencyInjector(IProcessConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public virtual void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IProcessConfiguration>(configuration);

            services.AddLogging(configure => { configure.AddConsole(); });

            services.AddSingleton(CloudStorageAccount.Parse(configuration.StorageAccount));

            services.AddSingleton<IEvaluacionDiferenciaMinimoMaximoService, EvaluacionDiferenciaMinimoMaximoService>(p =>
            {
                return new EvaluacionDiferenciaMinimoMaximoService(configuration.DiferenciaEntreMinimoMaximaSuperadaMessage, configuration.DifenciaMinimoMaximoAdmitida, p.GetService<ILoggerFactory>());
            });

            services.AddSingleton<IEvaluacionPromedioService, EvaluacionPromedioService>(p =>
             {
                 return new EvaluacionPromedioService(configuration.PromedioMaximoAdmitidoSuperadoMessage, configuration.PromedioMaximoAdmitido, p.GetService<ILoggerFactory>());
             });

            services.AddSingleton<IEvaluacionServiceComposite, EvaluacionServiceComposite>();

            services.AddSingleton<IMensajesService, MensajesService>(p =>
            {
                return new MensajesService(p.GetService<CloudStorageAccount>(), configuration.MessageQueueName);
            });
        }
    }
}