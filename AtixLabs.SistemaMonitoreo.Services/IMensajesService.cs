﻿using AtixLabs.SistemaMonitoreo.Model;
using System.Threading.Tasks;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public interface IMensajesService
    {
        Task<Mensaje> ObtenerMensajePorOrdenDeLlegadaAsync();

        Task GuardarMensajeAsync(Mensaje mensaje);
    }
}