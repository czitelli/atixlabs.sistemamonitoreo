﻿using AtixLabs.SistemaMonitoreo.Model;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AtixLabs.SistemaMonitoreo.Services
{
    public class MensajesService : IMensajesService
    {
        private readonly CloudQueue queue;

        public MensajesService(CloudStorageAccount cloudStorageAccount, String queueName) // Puedo usar IOptions<T>
        {
            var queueClient = cloudStorageAccount.CreateCloudQueueClient();

            this.queue = cloudStorageAccount.CreateCloudQueueClient().GetQueueReference(queueName);

            //Creo la queue en caso de no existir, solo para la prueba, la idea es que exista
            this.queue.CreateIfNotExistsAsync();
        }

        public async Task<Mensaje> ObtenerMensajePorOrdenDeLlegadaAsync()
        {
            var mensaje = await this.queue.GetMessageAsync();

            if (mensaje == null)
                return null;

            return await Task<Mensaje>.FromResult(JsonConvert.DeserializeObject<Mensaje>(mensaje.AsString));
        }

        public async Task GuardarMensajeAsync(Mensaje mensaje)
        {
            await this.queue.AddMessageAsync(new CloudQueueMessage(JsonConvert.SerializeObject(mensaje)));
        }
    }
}