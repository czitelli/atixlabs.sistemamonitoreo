using System;

namespace AtixLabs.SistemaMonitoreo.Functions
{
    [Microsoft.Azure.WebJobs.Description.Binding]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class InjectFactoryAttribute : Attribute
    {
        public InjectFactoryAttribute(Type type)
        {
            Type = type;
        }

        public Type Type { get; set; }
    }
}