﻿using AtixLabs.SistemaMonitoreo.Functions;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;

[assembly: WebJobsStartup(typeof(WebJobsExtensionStartup))]

namespace AtixLabs.SistemaMonitoreo.Functions
{
    public class WebJobsExtensionStartup : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.AddExtension<BaseInjectionConfiguration>();
        }
    }
}