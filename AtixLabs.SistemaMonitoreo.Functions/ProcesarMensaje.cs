using AtixLabs.SistemaMonitoreo.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AtixLabs.SistemaMonitoreo.Functions
{
    public static class ProcesarMensaje
    {
        //Funcion de Azure que procesa un
        [FunctionName("ProcesarMensaje")] //TiempoEjecucion Cada 5 Minutos puede cambiar a 30 para simular procesamiento
        public static async Task Run([TimerTrigger("%TiempoEjecucion%")]TimerInfo myTimer,
            ILogger log,
            [InjectFactory(typeof(IMensajesService))] IMensajesService mensajesService,
            [InjectFactory(typeof(IEvaluacionServiceComposite))] IEvaluacionServiceComposite evaluacionService)
        {
            //Desencolo mensaje de los 4 sensores
            var mensaje = await mensajesService.ObtenerMensajePorOrdenDeLlegadaAsync();

            if (mensaje != null)
            {
                //Logging de procesamiento (puedo usar archivo de recursos resx para los textos)
                log.LogInformation($"Se esta procesando el mensaje con fecha {mensaje.Fecha}");

                // Proceso Mensaje
                await evaluacionService.Evaluar(mensaje);

                //Logging de procesamiento
                log.LogInformation($"Se proceso el mensaje con fecha {mensaje.Fecha}");
            }
            else
            {
                log.LogInformation($"No se encontraron mensajes");
            }
        }
    }
}