﻿using AtixLabs.SistemaMonitoreo.Model;
using AtixLabs.SistemaMonitoreo.Services;
using Microsoft.Azure.WebJobs.Host.Config;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AtixLabs.SistemaMonitoreo.Functions
{
    public class BaseInjectionConfiguration : IExtensionConfigProvider
    {
        private static IServiceProvider _serviceProvider;

        public void Initialize(ExtensionConfigContext context)
        {
            var services = new ServiceCollection();
            RegisterServices(services);
            _serviceProvider = services.BuildServiceProvider(true);

            context
                .AddBindingRule<InjectFactoryAttribute>()
                .BindToInput<dynamic>(i => _serviceProvider.GetService(i.Type));
        }

        public virtual DependencyInjector GetInjector(IProcessConfiguration configuration)
        {
            return new DependencyInjector(configuration);
        }

        private void RegisterServices(IServiceCollection services)
        {
            var configuration = GetProcessConfiguration();
            var dependencyInjector = GetInjector(configuration);
            dependencyInjector.RegisterServices(services);
        }

        public static string GetSetting(string name)
        {
            return System.Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
        }

        public virtual IProcessConfiguration GetProcessConfiguration()
        {
            return new ProcessConfiguration
            {
                DifenciaMinimoMaximoAdmitida = Convert.ToInt32(GetSetting("DifenciaMinimoMaximoAdmitida")),
                DiferenciaEntreMinimoMaximaSuperadaMessage = GetSetting("DiferenciaEntreMinimoMaximaSuperadaMessage"),
                PromedioMaximoAdmitido = Convert.ToDecimal(GetSetting("PromedioMaximoAdmitido")),
                PromedioMaximoAdmitidoSuperadoMessage = GetSetting("PromedioMaximoAdmitidoSuperadoMessage"),
                MessageQueueName = GetSetting("MessageQueueName"),
                TiempoEjecucion = GetSetting("TiempoEjecucion"),
                StorageAccount = GetSetting("AzureWebJobsStorage")
            };
        }
    }
}