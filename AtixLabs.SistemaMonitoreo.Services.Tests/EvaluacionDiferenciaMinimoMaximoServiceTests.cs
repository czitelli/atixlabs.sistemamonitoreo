﻿using AtixLabs.SistemaMonitoreo.Model;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace AtixLabs.SistemaMonitoreo.Services.Tests
{
    [TestClass]
    public class EvaluacionDiferenciaMinimoMaximoServiceTests
    {
        protected EvaluacionDiferenciaMinimoMaximoService evaluacionDiferencia;

        protected String message;

        protected Int32 diferenciaAdmitida;

        protected Mock<ILoggerFactory> loggerFactoryMock;

        protected Mock<ILogger<IEvaluacionService>> loggerMock;

        [TestInitialize]
        public void Initialize()
        {
            this.message = "Maximo superado";

            this.diferenciaAdmitida = 3;

            this.loggerFactoryMock = new Mock<ILoggerFactory>();

            this.loggerMock = new Mock<ILogger<IEvaluacionService>>();

            this.loggerFactoryMock.Setup(l => l.CreateLogger("IEvaluacionService")).Returns(this.loggerMock.Object);

            this.evaluacionDiferencia = new EvaluacionDiferenciaMinimoMaximoService(this.message, this.diferenciaAdmitida, loggerFactoryMock.Object);
        }

        [TestClass]
        public class ElMetodo_Evaluar : EvaluacionDiferenciaMinimoMaximoServiceTests
        {
            [TestMethod]
            public void Retorna_True_Al_Evaluar_Si_La_Diferencia_Entre_Minimo_Y_Maximo_Es_Menor()
            {
                //Arrange
                var mensaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = 2,
                    Mediciones = new Int32[] { 1, 2, 3, 3 }
                };

                //Act
                var result = this.evaluacionDiferencia.Evaluar(mensaje).Result;

                //Assert
                Assert.IsTrue(result);
            }

            [TestMethod]
            public void Retorna_True_Al_Evaluar_Si_La_Diferencia_Entre_Minimo_Y_Maximo_Es_Igual_Al_Permitido()
            {
                //Arrange
                var mensaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = 2,
                    Mediciones = new Int32[] { 1, 2, 3, 4 }
                };

                //Act
                var result = this.evaluacionDiferencia.Evaluar(mensaje).Result;

                //Assert
                Assert.IsTrue(result);
            }

            [TestMethod]
            public void Retorna_False_Al_Evaluar_Si_La_Diferencia_Entre_Minimo_Y_Maximo_Es_Mayor()
            {
                //Arrange
                var mensaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = 2,
                    Mediciones = new Int32[] { 1, 2, 3, 6 }
                };

                //Act
                var result = this.evaluacionDiferencia.Evaluar(mensaje).Result;

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}