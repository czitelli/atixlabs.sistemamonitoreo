﻿using System;
using AtixLabs.SistemaMonitoreo.Model;
using AtixLabs.SistemaMonitoreo.Services;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AtixLabs.SistemaMonitoreo.Services.Tests
{
    [TestClass]
    public class EvaluacionPromedioServiceTests
    {
        protected EvaluacionPromedioService evaluacionPromedio;

        protected String message;

        protected Decimal promedioMaximo;

        protected Mock<ILoggerFactory> loggerFactoryMock;

        protected Mock<ILogger<IEvaluacionService>> loggerMock;

        [TestInitialize]
        public void Initialize()
        {
            this.message = "Maximo superado";

            this.promedioMaximo = 4;

            this.loggerFactoryMock = new Mock<ILoggerFactory>();

            this.loggerMock = new Mock<ILogger<IEvaluacionService>>();

            this.loggerFactoryMock.Setup(l => l.CreateLogger("IEvaluacionService")).Returns(this.loggerMock.Object);

            this.evaluacionPromedio = new EvaluacionPromedioService(this.message, this.promedioMaximo, loggerFactoryMock.Object);
        }

        [TestClass]
        public class ElMetodo_Evaluar : EvaluacionPromedioServiceTests
        {
            [TestMethod]
            public void Retorna_True_Al_Evaluar_Si_El_Promedio_Es_Menor()
            {
                //Arrange
                var mensaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = 2,
                    Mediciones = new Int32[] { 2, 2, 4, 4 }
                };

                //Act
                var result = this.evaluacionPromedio.Evaluar(mensaje).Result;

                //Assert
                Assert.IsTrue(result);
            }

            [TestMethod]
            public void Retorna_True_Al_Evaluar_Si_El_Promedio_Es_Igual_Al_Permitido()
            {
                //Arrange
                var mensaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = 2,
                    Mediciones = new Int32[] { 4, 4, 4, 4 }
                };

                //Act
                var result = this.evaluacionPromedio.Evaluar(mensaje).Result;

                //Assert
                Assert.IsTrue(result);
            }

            [TestMethod]
            public void Retorna_False_Al_Evaluar_Si_El_Promedio_Es_Mayor()
            {
                //Arrange
                var mensaje = new Mensaje()
                {
                    Fecha = DateTime.Now,
                    Id = 2,
                    Mediciones = new Int32[] { 4, 4, 5, 5 }
                };

                //Act
                var result = this.evaluacionPromedio.Evaluar(mensaje).Result;

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}